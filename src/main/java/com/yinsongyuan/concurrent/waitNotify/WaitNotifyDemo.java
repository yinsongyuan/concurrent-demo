package com.yinsongyuan.concurrent.waitNotify;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * wait notify测试Demo
 *
 * @author yinsongyuan
 * @date 2021/3/10
 **/
@Slf4j
public class WaitNotifyDemo {
    public static void main(String[] args) throws InterruptedException {
        waitNotifyTest();
    }

    static Thread t1, t2, t3;
    static Object LOCK = new Object();

    public static void waitNotifyTest() throws InterruptedException {
        t1 = new Thread(() -> {
            synchronized (LOCK) {
                while (true) {
                    try {
                        LOCK.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    log.info("t1 do something……");
                }
            }
        });
        t1.start();
        t2 = new Thread(() -> {
            synchronized (LOCK) {
                log.info("t2 do something……");
            }
        });
        t2.start();
        TimeUnit.MILLISECONDS.sleep(100);
        log.info("唤醒其他线程");
        synchronized (LOCK) {
            LOCK.notify();
        }
    }
}
