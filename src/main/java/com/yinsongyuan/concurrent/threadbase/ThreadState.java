package com.yinsongyuan.concurrent.threadbase;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * 查看各个状态下的线程状态
 *
 * @author yinsongyuan
 * @date 2021/3/2
 **/
@Slf4j(topic = "thread.state")
public class ThreadState {
    public static void main(String[] args) {
//        log.info("已创建的线程:{}", createThread());
//        log.info("运行中的线程:{}", startThread());
//        log.info("运行完成的线程:{}", joinThread());
//        log.info("睡觉中的线程:{}", sleepThread());
        log.info("等待锁的线程:{}", synchronizedThread());
    }

    public static String createThread() {
        Thread thread = new Thread(() -> log.info("永远不会启动的线程!"));
        return thread.getState().name();
    }

    public static String startThread() {
        Thread thread = new Thread(() -> {
            log.info("正常启动的线程!");
            while (true) {

            }
        });
        thread.start();
        return thread.getState().name();
    }

    public static String joinThread() {
        Thread thread = new Thread(() -> {
            log.info("运行完成的线程!");
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return thread.getState().name();
    }

    public static String sleepThread() {
        Thread thread = new Thread(() -> {
            log.info("睡觉中的线程!");
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread.start();
        return thread.getState().name();
    }

    public static String synchronizedThread() {
        String lockStr = "LOCK";
        Thread t1 = new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (lockStr) {
                log.info("不可能执行的日志打印!");
            }
        }, "不让获取锁线程");
        Thread t2 = new Thread(() -> {
            synchronized (lockStr) {
                while (true) {

                }
            }
        }, "强锁线程");
        t2.start();
        t1.start();
        return t1.getState().name();
    }

}
