package com.yinsongyuan.concurrent.threadbase;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * 测试线程join方法
 *
 * @author yinsongyuan
 * @date 2021/3/10
 **/
@Slf4j
public class ThreadJoin {

    public static void main(String[] args) throws InterruptedException {
        // t1 t2 main
        // 执行顺序 t2 t1 main
        Thread t1 = new Thread(() -> {
            log.info("进入线程1");
            log.info("t1 do something");
            log.info("结束线程");
        }, "t1");
        Thread t2 = new Thread(() -> {
            log.info("进入线程2");
            try {
                t1.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("t2 do something");
            log.info("结束线程");
        }, "t2");
        t1.start();
        t2.start();
        log.info("main线程执行");
    }
}
