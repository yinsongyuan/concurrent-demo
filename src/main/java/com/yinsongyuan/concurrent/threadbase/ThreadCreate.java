package com.yinsongyuan.concurrent.threadbase;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 * @author yinsongyuan
 * @date 2021/3/1
 * @desc
 * 线程创建方式
 * 1.线程创建方式一: 重写Thread类的run方法 {@link ThreadCreate#createThread}
 * 2.线程创建方式二: 实现Runnable接口的run方法 {@link ThreadCreate#createThreadByRunnable}
 * 3.线程创建方式三: 实现Callable接口的run方法 {@link ThreadCreate#createThreadByFutureTask}
 **/
@Slf4j(topic = "thread.create")
public class ThreadCreate {

    public static void main(String[] args) {
        createThread();
//        createThreadByRunnable();
//        createThreadByFutureTask();
    }

    public static void createThread() {
        Thread t1 = new Thread() {
            @Override
            public void run() {
                int i = 1/0;
                log.info("线程创建方式一:重写Thread类run方法");
            }
        };
        t1.setName("ThreadCreate1");
        t1.start();
        // Lamda实现
        Thread t2 = new Thread(() -> log.info("线程创建方式一:重写Thread类run方法(Lamda语法)"));
        t2.setName("ThreadCreate2");
        t2.start();
    }

    public static void createThreadByRunnable() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                log.info("线程创建方式二:实现Runnable接口中run方法");
            }
        };
        Thread t1 = new Thread(runnable, "RunableCreate1");
        t1.start();
        // Lamda实现
        Thread t2 = new Thread(() -> log.info("线程创建方式二:实现Runnable接口中run方法(Lamda语法)"), "RunnableCreate2");
        t2.start();
    }

    public static void createThreadByFutureTask() {
        Callable callable = new Callable() {
            @Override
            public Object call() throws Exception {
                log.info("创建线程方式三:实现callable接口中call方法");
                return null;
            }
        };
        FutureTask<Object> futureTask1 = new FutureTask<>(callable);
        Thread t1 = new Thread(futureTask1, "FutureTask1");
        t1.start();
        // Lamda实现
        FutureTask<Object> futureTask2 = new FutureTask<>(() -> {
            log.info("创建线程方式三:实现callable接口中call方法(Lamda实现)");
            return null;
        });
        Thread t2 = new Thread(futureTask2, "FutureTask2");
        t2.start();
    }

}
