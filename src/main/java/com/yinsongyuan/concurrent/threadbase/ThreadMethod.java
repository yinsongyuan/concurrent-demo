package com.yinsongyuan.concurrent.threadbase;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * @author yinsongyuan
 * @date 2021/3/2
 * @description 线程常见方法
 **/
@Slf4j(topic = "thread.method")
public class ThreadMethod {

    public static void main(String[] args) {
//        startFunctionDemo();
//        runFunctionDemo();
//        joinFunctionDemo();
//        getFunctionDemo();
//        sleepFunctionDemo();
//        yieldFunctionDemo();
//        interruptFunctionDemo();
//        parkFunctionDemo();
    }

    /**
     * @author yinsongyuan
     * @date 2021/3/2
     * @description start() 启动线程
     **/
    public static void startFunctionDemo() {
        Thread thread = new Thread(() -> log.info("Thread.star() Demo"), "startThread");
        thread.start();
    }

    /**
     * @author yinsongyuan
     * @date 2021/3/2
     * @description run() 主线程调用run()
     **/
    public static void runFunctionDemo() {
        Thread thread = new Thread(() -> log.info("Thread.run() Demo"), "runThread");
        thread.run();
    }

    /**
     * @author yinsongyuan
     * @date 2021/3/2
     * @dscription join() 等待线程运行结束()
     **/
    public static void joinFunctionDemo() {
        Thread joinThread1 = new Thread(() -> {
            log.info("Thread.join() Demo1");
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "joinThread1");
        joinThread1.start();
        // 等待线程运行结束
        try {
            joinThread1.join();
            log.info("joinThread1线程运行结束");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Thread joinThread2 = new Thread(() -> {
            log.info("Thread.join() Demo2");
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "joinThread2");
        joinThread2.start();
        // 等待线程运营结束,最大等待时间n
        try {
            joinThread2.join(1000);
            log.info("joinThread2线程运行结束");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * @author yinsongyuan
     * @date 2021/3/2
     * @description 获取线程属性信息
     **/
    public static void getFunctionDemo() {
        Thread thread = new Thread(() -> log.info("Thread.run() Demo"), "runThread");
        log.info("线程ID:{}", thread.getId());
        log.info("线程名称:{}", thread.getName());
        log.info("线程优先级:{}", thread.getPriority());
        log.info("线程状态:{}", thread.getState());
        log.info("线程打断标记:{}", thread.isInterrupted());
        log.info("线程存活标记:{}", thread.isAlive());
    }

    /**
     * @author yinsongyuan
     * @date 2021/3/2
     * @dscription 线程休眠, 休眠时 让出CPU的时间片给其他线程
     **/
    public static void sleepFunctionDemo() {
        Thread thread = new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("thread");
        }, "Thread");
        thread.start();
        log.info("睡眠状态:{}", thread.getState());
    }

    /**
     * @author yinsongyuan
     * @date 2021/3/2
     * @description 提示当前线程调度器, 让出当前线程对CPU的使用
     **/
    public static void yieldFunctionDemo() {
        // 线程优先级测试
        Thread yieldThread1 = new Thread(() -> {
            int count = 0;
            for (; ; ) {
                Thread.yield();
                log.info("线程1:{}", count++);
            }
        }, "yieldThread1");
        Thread yieldThread2 = new Thread(() -> {
            int count = 0;
            for (; ; ) {
                log.info("线程2:{}", count++);
            }
        }, "yieldThread2");
        yieldThread1.start();
        yieldThread2.start();
    }

    /**
     * @author yinsongyuan
     * @date 2021/3/2
     * @description 打断线程
     **/
    public static void interruptFunctionDemo() {
        // 打断普通线程
        Thread thread = new Thread(() -> {
            while (true) {
                // 获取当前线程
                Thread currentThread = Thread.currentThread();
                if (currentThread.isInterrupted()) {
                    log.info("当前线程被打断!:{}", currentThread.isInterrupted());
//                    break;
                }
                // Thread.interrupted() 使用完之后 清除打断标记
                if (Thread.interrupted()) {
                    log.info("当前线程被打断:{}", currentThread.isInterrupted());
                    break;
                }
            }
        }, "thread");
        thread.start();
        log.info("普通线程打断之前:{}", thread.isInterrupted());
        thread.interrupt();
        log.info("普通线程打断之后:{}", thread.isInterrupted());
        // 打断睡眠线程
        Thread sleepThread = new Thread(() -> {
            log.info("睡眠2秒");
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                log.info("sleep被打断");
                e.printStackTrace();
            }
            log.info("sleep被打断后的 线程标记:{}", Thread.currentThread().isInterrupted());
        });
        sleepThread.start();
        sleepThread.interrupt();
    }

    /**
     * @author yinsongyuan
     * @date 2021/3/2
     * @descrption 测试线程中的park
     **/
    public static void parkFunctionDemo() {
        Thread thread = new Thread(() -> {
            log.info("进入线程");
            LockSupport.park();
            log.info("没被打断！！！");
        }, "parkThread");
        thread.start();
    }
}
