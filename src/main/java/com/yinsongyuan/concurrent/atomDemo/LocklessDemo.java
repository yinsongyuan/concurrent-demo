package com.yinsongyuan.concurrent.atomDemo;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class LocklessDemo {
    public static final int BALANCE = 50000;

    public static void main(String[] args) {
        Account accountUnsafe = new AccountUnsafe();
        log.info("初始值:{}", accountUnsafe.getBalance());
        test(accountUnsafe);
        log.info("结束值:{}", accountUnsafe.getBalance());

        Account synchroizedAccount = new AccountSynchroized();
        log.info("初始值:{}", synchroizedAccount.getBalance());
        test(synchroizedAccount);
        log.info("结束值:{}", synchroizedAccount.getBalance());

        Account accountAtom = new AccountAtom();
        log.info("初始值:{}", accountAtom.getBalance());
        test(accountAtom);
        log.info("结束值:{}", accountAtom.getBalance());
    }

    public static void test(Account account) {
        List<Thread> ts = new ArrayList<>();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < BALANCE; i++) {
            Thread thread = new Thread(() -> {
                try {
                    TimeUnit.MILLISECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                account.withdrawal();
            });
            thread.setName("test" + i);
            ts.add(thread);
            thread.start();
        }
        ts.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        long endTime = System.currentTimeMillis();
        log.info("运行时间:{}", endTime - startTime);
    }

}

interface Account {

    Integer getBalance();

    void withdrawal();

}

@Slf4j
class AccountUnsafe implements Account {

    private Integer balance = LocklessDemo.BALANCE;

    @Override
    public Integer getBalance() {
        return this.balance;
    }

    @Override
    public void withdrawal() {
        balance--;
    }
}

@Slf4j
class AccountSynchroized implements Account {
    private Integer balance = LocklessDemo.BALANCE;

    private Object lock = new Object();

    @Override
    public Integer getBalance() {
        synchronized (lock) {
            return this.balance;
        }
    }

    @Override
    public void withdrawal() {
        synchronized (lock) {
            balance--;
        }
    }
}

@Slf4j
class AccountAtom implements Account {

    private AtomicInteger balance = new AtomicInteger(LocklessDemo.BALANCE);

    @Override
    public Integer getBalance() {
        return this.balance.get();
    }

    @Override
    public void withdrawal() {
        while (true) {
            int prev = balance.get();
            int next = prev - 1;
//            balance.decrementAndGet();
            if (balance.compareAndSet(prev, next)) {
                break;
            }
        }
    }
}