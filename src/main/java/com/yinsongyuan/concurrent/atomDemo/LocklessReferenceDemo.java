package com.yinsongyuan.concurrent.atomDemo;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 测试使用原子引用的安全性
 *
 * @author yinsongyuan
 * @date 2021/3/18
 **/
@Slf4j
public class LocklessReferenceDemo {
    public static final BigDecimal BALANCE = new BigDecimal(5000);

    public static void main(String[] args) {
        AccountReference accountUnsafeReference = new AccountUnsafeReference();
        concurrentTest(accountUnsafeReference);

        AccountReference accountSynchronizedReference = new AccountSynchronizedReference();
        concurrentTest(accountSynchronizedReference);

        AccountReference accountAtomicReference = new AccountAtomicReference();
        concurrentTest(accountAtomicReference);
    }

    static void concurrentTest(AccountReference accountReference) {
        List<Thread> ts = new ArrayList<>();
        log.info("初始金额:{}", accountReference.getAccount());
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 5000; i++) {
            Thread thread = new Thread(() -> {
                accountReference.withdrawal();
            }, "线程" + i);
            thread.start();
            ts.add(thread);
        }
        ts.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        long endTime = System.currentTimeMillis();
        log.info("结束金额:{} 执行时间:{}", accountReference.getAccount(), endTime - startTime);
    }
}

/**
 * 多线程测试接口
 *
 * @author yinsongyuan
 * @date 2021/3/18
 **/
interface AccountReference {
    BigDecimal getAccount();

    void withdrawal();
}

/**
 * 不安全实现类
 *
 * @author yinsongyuan
 * @date 2021/3/18
 **/
class AccountUnsafeReference implements AccountReference {
    private BigDecimal balance = LocklessReferenceDemo.BALANCE;

    @Override
    public BigDecimal getAccount() {
        return this.balance;
    }

    @Override
    public void withdrawal() {
        balance = balance.subtract(BigDecimal.ONE);
    }
}

class AccountSynchronizedReference implements AccountReference {
    private BigDecimal balance = LocklessReferenceDemo.BALANCE;
    private Object lock = new Object();

    @Override
    public BigDecimal getAccount() {
        synchronized (lock) {
            return this.balance;
        }
    }

    @Override
    public void withdrawal() {
        synchronized (lock) {
            balance = balance.subtract(BigDecimal.ONE);
        }
    }
}

class AccountAtomicReference implements AccountReference {
    private AtomicReference<BigDecimal> balance = new AtomicReference<>(LocklessReferenceDemo.BALANCE);

    @Override
    public BigDecimal getAccount() {
        return this.balance.get();
    }

    @Override
    public void withdrawal() {
//        balance.set(balance.get().subtract(BigDecimal.ONE));
        while (true) {
            BigDecimal pre = balance.get();
            BigDecimal next = pre.subtract(BigDecimal.ONE);
            if (balance.compareAndSet(pre, next)) {
                break;
            }
        }
    }
}