package com.yinsongyuan.concurrent.atomDemo;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Atomic Api Demo
 *
 * @author yinsongyuan
 * @date 2021/3/18
 **/
@Slf4j
public class AtomicApiDemo {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(0);
        int getAndIncrement = atomicInteger.getAndIncrement();
        log.info("自增返回自增之前的值 getAndIncrement:{}", getAndIncrement);
        int incrementAndGet = atomicInteger.incrementAndGet();
        log.info("自增返回自增之后的值 incrementAndGet:{}", incrementAndGet);
        int getAndDecrement = atomicInteger.getAndDecrement();
        log.info("自减返回自减之前的值 getAndDecrement:{}", getAndDecrement);
        int decrementAndGet = atomicInteger.decrementAndGet();
        log.info("自减返回自减之后的值 decrementAndGet:{}", decrementAndGet);

        int addAndGet = atomicInteger.addAndGet(5);
        log.info("自定义加返回操作之后的值 addAndGet:{}", addAndGet);

        int updateAndGet = atomicInteger.updateAndGet(i -> i / 5);
        log.info("自定义计算函数 updateAndGet:{}", updateAndGet);

        int accumulateAndGet = atomicInteger.accumulateAndGet(10, (p, x) -> {
            int o = p + x;
            return o;
        });
        log.info("增加自定义入参和函数 accumulateAndGet:{}", accumulateAndGet);
    }
}
