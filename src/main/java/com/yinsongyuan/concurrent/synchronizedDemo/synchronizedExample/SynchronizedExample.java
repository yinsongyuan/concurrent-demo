package com.yinsongyuan.concurrent.synchronizedDemo.synchronizedExample;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * 对象锁相关案例
 *
 * @author yinsongyuan
 * @date 2021/3/3
 **/
@Slf4j(topic = "synchronized.example")
public class SynchronizedExample {
    public static void main(String[] args) {
//        synchronizedExample1();
//        synchronizedExample2();
//        synchronizedExample3();
//        synchronizedExample4();
//        synchronizedExample5();
//        synchronizedExample6();
        synchronizedExample7();
        synchronizedExample8();
    }

    public static void synchronizedExample1() {
        Number1 number1 = new Number1();
        new Thread(() -> number1.a()).start();
        new Thread(() -> number1.b()).start();
    }

    public static void synchronizedExample2() {
        Number2 number2 = new Number2();
        new Thread(() -> number2.a()).start();
        new Thread(() -> number2.b()).start();
    }

    public static void synchronizedExample3() {
        Number2 number2 = new Number2();
        new Thread(() -> number2.a()).start();
        new Thread(() -> number2.b()).start();
        new Thread(() -> number2.c()).start();
        // c 1s ab 或者 cb 1s a 或者 bc 1s a
    }

    public static void synchronizedExample4() {
        Number2 n1 = new Number2();
        Number2 n2 = new Number2();
        new Thread(() -> n1.a()).start();
        new Thread(() -> n2.b()).start();
        // b 1s a
    }

    public static void synchronizedExample5() {
        log.info("进入方法");
        Number3 number3 = new Number3();
        new Thread(() -> Number3.a()).start();
        new Thread(() -> number3.b()).start();
        // b 1s a 或者 1s ab !!!! ***.class 和 它的实例化对象 不是同一个锁
        // 正确结果 b 1s a;
    }

    public static void synchronizedExample6() {
        new Thread(() -> Number4.a()).start();
        new Thread(() -> Number4.b()).start();
        // 1s ab 或者 b 1s a
    }

    public static void synchronizedExample7() {
        log.info("进入方法");
        Number3 n1 = new Number3();
        Number3 n2 = new Number3();
        new Thread(() -> Number3.a()).start();
        new Thread(() -> n2.b()).start();
        // b 1s a
    }

    public static void synchronizedExample8() {
        new Thread(() -> Number4.a()).start();
        new Thread(() -> Number4.b()).start();
        // 1s ab 或者 b 1s a
    }
}

@Slf4j(topic = "synchronized.number1")
class Number1 {
    public synchronized void a() {
        log.info("a");
    }

    public synchronized void b() {
        log.info("b");
    }
}

@Slf4j(topic = "synchronized.number2")
class Number2 {
    public synchronized void a() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("a");
    }

    public synchronized void b() {
        log.info("b");
    }

    public void c() {
        log.info("c");
    }
}

@Slf4j(topic = "synchronized.number3")
class Number3 {
    public static synchronized void a() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("a");
    }

    public synchronized void b() {
        log.info("b");
    }
}

@Slf4j(topic = "synchronized.number4")
class Number4 {
    public static synchronized void a() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("a");
    }

    public static synchronized void b() {
        log.info("b");
    }
}
