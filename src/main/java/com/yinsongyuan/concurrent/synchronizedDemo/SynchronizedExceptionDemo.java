package com.yinsongyuan.concurrent.synchronizedDemo;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * 测试异常情况下 是否会释放锁
 *
 * @author yinsongyuan
 * @date 2021/3/10
 **/
@Slf4j
public class SynchronizedExceptionDemo {
    static Object LOCK = new Object();

    public static void main(String[] args) {
        new Thread(() -> {
            synchronized (LOCK) {
                log.info("进入线程t1");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                int i = 1 / 0;
            }
        }, "t1").start();
        new Thread(() -> {
            synchronized (LOCK) {
                log.info("进入线程t2");
            }
        }, "t2").start();

    }
}
