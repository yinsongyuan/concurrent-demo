package com.yinsongyuan.concurrent.synchronizedDemo;

import lombok.extern.slf4j.Slf4j;

/**
 * synchronized 对象锁简单使用
 *
 * @author yinsongyuan
 * @date 2021/3/3
 **/
@Slf4j(topic = "synchronized.base")
public class BaseSynchronizedDemo {
    public static void main(String[] args) {
        log.info("不加对象锁测试:{}", concurrentTest());
        log.info("加对象锁测试:{}", synchronizedTestDemo());
    }

    /**
     * 共享变量
     */
    static int count = 0;
    /**
     * 多线程操作同一个共享变量
     * result: 加减同样次数 结果不一定为零
     * @author yinsongyuan
     * @date 202/3/3
     **/
    public static int concurrentTest() {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 5000; i++) {
                count++;
            }
        });
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 5000; i++) {
                count--;
            }
        });
        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * 多线程操作加同步锁的对象
     * result: 加减同样次数 多次测试 结果都为零
     * @author yinsongyuan
     * @date 2021/3/3
     */
    public static int synchronizedTestDemo() {
        SynchronizedCount synchronizedCount = new SynchronizedCount();
        SynchronizedCount synchronizedCount1 = new SynchronizedCount();
        SynchronizedCount synchronizedCount2 = new SynchronizedCount();
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                synchronizedCount1.increment();
            }
        });
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                synchronizedCount1.decrement();
            }
        });
        Thread t3 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                synchronizedCount2.increment();
            }
        });
        Thread t4 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                synchronizedCount2.decrement();
            }
        });
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return synchronizedCount.get();
    }

}

/**
 * 对象锁测试类
 *
 * @author yinsongyuan
 * @date 2021/3/3
 **/
class SynchronizedCount {
    /**
     * 共享变量
     */
    private static int count = 0;

    public void increment() {
        synchronized (SynchronizedCount.class) {
            count++;
        }
    }

    public void decrement() {
        synchronized (SynchronizedCount.class) {
            count--;
        }
    }

    public int get() {
        synchronized (SynchronizedCount.class) {
            return count;
        }
    }
}