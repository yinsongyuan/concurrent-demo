package com.yinsongyuan.concurrent.pool;

import com.sun.corba.se.spi.orbutil.threadpool.Work;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.jni.Time;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 自己实现的线程池
 *
 * @author yinsongyuan
 * @date 2021/3/21
 */
@Slf4j
public class ImplementationThreadPool {
    public static void main(String[] args) {
        YsyThreadPool ysyThreadPool = new YsyThreadPool(1, 1, TimeUnit.SECONDS, 1, (queue, task) -> {
            // 1. 继续等待
//            queue.put(task);
            //2. 超时等待
//            queue.put(task, 1, TimeUnit.SECONDS);
            //3. 直接不处理啦
//            log.info("不用执行了 太开心了");
            //4. 异常
            //5. 自己调用处理
        });
        for (int i = 0; i < 3; i++) {
            int j = i;
            Runnable task = () -> {
                try {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.info("任务{}:do something bussiness……", j);
            };
            ysyThreadPool.execute(task, j);
        }
    }
}

@FunctionalInterface
interface RejectPolicy<T> {
    void reject(YsyBlockingQueue ysyBlockingQueue, T task);
}

@Slf4j
class YsyThreadPool {

    static int WORKER_ID = 0;
    /**
     * 任务队列
     */
    private YsyBlockingQueue<Runnable> ysyBlockingQueue;
    /**
     * 线程集合
     */
    private HashSet<Worker> workers = new HashSet<>();
    /**
     * 核心线程数
     */
    private int coreSize;
    /**
     * 线程超时时间
     */
    private long timeOut;
    /**
     * 时间单位
     */
    private TimeUnit timeUnit;
    /**
     * 拒绝策略
     */
    private RejectPolicy rejectPolicy;

    public void execute(Runnable task, int i) {
        log.info("线程执行集合{}", workers.size());
        if (workers.size() < coreSize) {
            log.info("任务{}:未到核心线程数 创建线程并执行", i);
            synchronized (workers) {
                Worker worker = new Worker(task);
                workers.add(worker);
                worker.start();
            }
        } else {
            log.info("任务{}:核心线程数已满加入队列", i);
            ysyBlockingQueue.tryPut(rejectPolicy, task);
        }

    }

    public YsyThreadPool(int coreSize, long timeOut, TimeUnit timeUnit, int capacity, RejectPolicy<Runnable> rejectPolicy) {
        this.coreSize = coreSize;
        this.timeOut = timeOut;
        this.timeUnit = timeUnit;
        this.rejectPolicy = rejectPolicy;
        ysyBlockingQueue = new YsyBlockingQueue<>(capacity);
    }

    class Worker extends Thread {

        private int id;

        private Runnable task;

        public Worker(Runnable task) {
            this.id = WORKER_ID++;
            this.task = task;
        }

        @Override
        public void run() {
            while (null != task || (task = ysyBlockingQueue.take(1, TimeUnit.SECONDS)) != null) {
                try {
                    log.info("线程{}:执行任务:{}", id, task);
                    task.run();
                } finally {
                    task = null;
                }
            }
            synchronized (workers) {
                log.info("线程{}:获取超时触发移除操作", id);
                workers.remove(this);
            }
        }
    }
}

/**
 * 自定义阻塞队列
 *
 * @author yinsongyuan
 * @date 2021/3/21
 **/
@Slf4j
class YsyBlockingQueue<T> {
    /**
     * 阻塞队列
     */
    private Deque<T> deque = new ArrayDeque<>();
    /**
     * 队列大小
     */
    private int capacity;
    /**
     * 队列获取/添加操作 锁
     * 保证线程安全
     */
    private ReentrantLock lock = new ReentrantLock();
    /**
     * 队列已满 生产等待 条件变量
     */
    private Condition fullWaitSet = lock.newCondition();
    /**
     * 空队列  消费等待 条件变量
     */
    private Condition emptyWaitSet = lock.newCondition();

    public YsyBlockingQueue(int capacity) {
        this.capacity = capacity;
    }

    /**
     * 添加任务方法(生产)
     *
     * @param task 任务
     * @author yinsongyuan
     * @date 2021/3/21
     */
    @SneakyThrows
    public void put(T task) {
        lock.lock();
        try {
            // 当前队列大小等于容量说明队列已满 进行生产等待
            while (size() == capacity) {
                log.info("等待加入任务队列");
                fullWaitSet.await();
            }
            deque.addLast(task);
            emptyWaitSet.signal();
            log.info("put task {} and queue size {}", task, deque.size());
        } finally {
            lock.unlock();
        }
    }

    /**
     * 添加任务方法(生产)(带超时时间)
     *
     * @param task 任务
     * @return boolean 是否添加添加成功
     * @author yinsongyuan
     * @date 2021/3/21
     */
    @SneakyThrows
    public boolean put(T task, long timeOut, TimeUnit timeUnit) {
        log.info("超时等待put");
        lock.lock();
        try {
            // 当前队列大小等于容量说明队列已满 进行生产等待
            long nanos = timeUnit.toNanos(timeOut);
            while (size() == capacity) {
                if (nanos <= 0) {
                    log.info("添加任务超时");
                    return false;
                }
                log.info("等待加入任务队列");
                nanos = fullWaitSet.awaitNanos(nanos);
            }
            deque.addLast(task);
            emptyWaitSet.signal();
            log.info("put task {} and queue size {}", task, deque.size());
        } finally {
            lock.unlock();
        }
        return true;
    }

    /**
     * 拒绝策略
     */
    public void tryPut(RejectPolicy<T> rejectPolicy, T task) {
        lock.lock();
        try {
            // 当前队列大小等于容量说明队列已满 进行生产等待
            log.info("{}---{}", size(), capacity);
            if (size() == capacity) {
                rejectPolicy.reject(this, task);
            } else {
                log.info("加入 task {} and queue size {}", task, deque.size());
                deque.addLast(task);
                emptyWaitSet.signal();
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * 获取任务方法(消费)
     *
     * @author yinsongyuan
     * @date 2021/3/21
     */
    @SneakyThrows
    public T take() {
        lock.lock();
        try {
            while (isEmpty()) {
                emptyWaitSet.await();
            }
            T t = deque.removeFirst();
            fullWaitSet.signal();
            log.info("take task {} and queue size {}", t, deque.size());
            return t;
        } finally {
            lock.unlock();
        }
    }

    /**
     * 获取任务方法(带超时)
     *
     * @author yinsongyuan
     * @date 2021/3/21
     */
    @SneakyThrows
    public T take(long timeOut, TimeUnit timeUnit) {
        lock.lock();
        try {
            long nacas = timeUnit.toNanos(timeOut);
            while (isEmpty()) {
                if (nacas <= 0) {
                    return null;
                }
                nacas = emptyWaitSet.awaitNanos(nacas);
            }
            T t = deque.removeFirst();
            fullWaitSet.signal();
            log.info("take task {} and queue size {}", t, deque.size());
            return t;
        } finally {
            lock.unlock();
        }
    }

    public int size() {
        return deque.size();
    }

    public boolean isEmpty() {
        return size() == 0 ? true : false;
    }
}

