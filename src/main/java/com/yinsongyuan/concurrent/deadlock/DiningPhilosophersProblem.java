package com.yinsongyuan.concurrent.deadlock;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalTime;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

/**
 * 哲学家就餐问题复现
 *
 * @author yinsongyuan
 * @date 2021/3/8
 **/
@Slf4j
public class DiningPhilosophersProblem {
    /**
     * 1~5
     */
    public static LinkedList<Chopstick> CHOPSTICK_LINKEDLIST = new LinkedList();
    /**
     * 1~5
     */
    public static LinkedList PHILOSPHER_LINKEDLIST = new LinkedList();

    static Chopstick chopstick1 = new Chopstick(1);
    static Chopstick chopstick2 = new Chopstick(2);
    static Chopstick chopstick3 = new Chopstick(3);
    static Chopstick chopstick4 = new Chopstick(4);
    static Chopstick chopstick5 = new Chopstick(5);

    static {

        CHOPSTICK_LINKEDLIST.add(chopstick1);
        CHOPSTICK_LINKEDLIST.add(chopstick2);
        CHOPSTICK_LINKEDLIST.add(chopstick3);
        CHOPSTICK_LINKEDLIST.add(chopstick4);
        CHOPSTICK_LINKEDLIST.add(chopstick5);

        PHILOSPHER_LINKEDLIST.add(1);
        PHILOSPHER_LINKEDLIST.add(2);
        PHILOSPHER_LINKEDLIST.add(3);
        PHILOSPHER_LINKEDLIST.add(4);
        PHILOSPHER_LINKEDLIST.add(5);
    }

    public static void main(String[] args) {
        for (int i = 1; i <= PHILOSPHER_LINKEDLIST.size(); i++) {
            Philosopher philospher = new Philosopher(i);
            philospher.setName("phi" + i);
            philospher.start();
        }
    }
}

@Slf4j
@NoArgsConstructor
@AllArgsConstructor
class Chopstick {
    /**
     * id
     * 1~5
     */
    public long id;
}

@Slf4j
class Philosopher extends Thread {
    /**
     * id
     * 1~5
     */
    private long id;
    /**
     * 左手
     */
    private volatile PhiHand left = new PhiHand(true);
    /**
     * 右手
     */
    private volatile PhiHand right = new PhiHand(false);

    public Philosopher(long id) {
        this.id = id;
    }

    void eat() {
        while (true) {
            if (left.ChopstickFlag && right.ChopstickFlag) {
                log.info("{} eat…… release {} {} ", id, LocalTime.now());
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                left.ChopstickFlag = false;
                right.ChopstickFlag = false;
                break;
            }
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            left.getChopstick(this.id);

            right.getChopstick(this.id);

            eat();

            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @NoArgsConstructor
    @AllArgsConstructor
    class PhiHand {
        /**
         * TRUE left FALSE right
         */
        boolean LeftFlag;

        volatile boolean ChopstickFlag = false;

        public PhiHand(boolean leftFlag) {
            LeftFlag = leftFlag;
        }

        public Chopstick getChopstick(long phiId) {
            if (this.LeftFlag) {
                synchronized (DiningPhilosophersProblem.CHOPSTICK_LINKEDLIST.get((int) (phiId - 1))) {
                    log.info("{} get {}", phiId, DiningPhilosophersProblem.CHOPSTICK_LINKEDLIST.get((int) (phiId - 1)).id);
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    this.ChopstickFlag = true;
                    return DiningPhilosophersProblem.CHOPSTICK_LINKEDLIST.get((int) (phiId - 1));
                }
            } else {
                int index = phiId == 5 ? 0 : (int) phiId;
                synchronized (DiningPhilosophersProblem.CHOPSTICK_LINKEDLIST.get(index)) {
                    log.info("{} get {} {}", phiId, DiningPhilosophersProblem.CHOPSTICK_LINKEDLIST.get(index).id, LocalTime.now());
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    this.ChopstickFlag = true;
                    return DiningPhilosophersProblem.CHOPSTICK_LINKEDLIST.get(index);
                }
            }
        }
    }
}

