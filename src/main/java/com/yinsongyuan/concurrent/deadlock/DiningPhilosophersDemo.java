package com.yinsongyuan.concurrent.deadlock;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
public class DiningPhilosophersDemo {
    public static void main(String[] args) {
//        ChopstickDemo chopstick1 = new ChopstickDemo();
//        ChopstickDemo chopstick2 = new ChopstickDemo();
//        ChopstickDemo chopstick3 = new ChopstickDemo();
//        ChopstickDemo chopstick4 = new ChopstickDemo();
//        ChopstickDemo chopstick5 = new ChopstickDemo();
//
//        PhilosopherDemo philosopher1 = new PhilosopherDemo(chopstick1, chopstick2);
//        PhilosopherDemo philosopher2 = new PhilosopherDemo(chopstick2, chopstick3);
//        PhilosopherDemo philosopher3 = new PhilosopherDemo(chopstick3, chopstick4);
//        PhilosopherDemo philosopher4 = new PhilosopherDemo(chopstick4, chopstick5);
//        PhilosopherDemo philosopher5 = new PhilosopherDemo(chopstick5, chopstick1);

        ReentrantLock reentrantLock1 = new ReentrantLock();
        ReentrantLock reentrantLock2 = new ReentrantLock();
        ReentrantLock reentrantLock3 = new ReentrantLock();
        ReentrantLock reentrantLock4 = new ReentrantLock();
        ReentrantLock reentrantLock5 = new ReentrantLock();

        PhilosopherDemo philosopher1 = new PhilosopherDemo(reentrantLock1, reentrantLock2);
        PhilosopherDemo philosopher2 = new PhilosopherDemo(reentrantLock2, reentrantLock3);
        PhilosopherDemo philosopher3 = new PhilosopherDemo(reentrantLock3, reentrantLock4);
        PhilosopherDemo philosopher4 = new PhilosopherDemo(reentrantLock4, reentrantLock5);
        PhilosopherDemo philosopher5 = new PhilosopherDemo(reentrantLock5, reentrantLock1);


        philosopher1.setName("philosopher1");
        philosopher2.setName("philosopher2");
        philosopher3.setName("philosopher3");
        philosopher4.setName("philosopher4");
        philosopher5.setName("philosopher5");
        philosopher1.start();
        philosopher2.start();
        philosopher3.start();
        philosopher4.start();
        philosopher5.start();
    }
}

@Slf4j
@NoArgsConstructor
class PhilosopherDemo extends Thread {

    private ChopstickDemo left;

    private ChopstickDemo right;

    private ReentrantLock leftReentrantLock;

    private ReentrantLock rightReentrantLock;

    public PhilosopherDemo(ChopstickDemo left, ChopstickDemo right) {
        this.left = left;
        this.right = right;
    }

    public PhilosopherDemo(ReentrantLock leftReentrantLock, ReentrantLock rightReentrantLock) {
        this.leftReentrantLock = leftReentrantLock;
        this.rightReentrantLock = rightReentrantLock;
    }

    @Override
    public void run() {
        while (true) {
//            eatSynchronized();
            eatReentrantLock();
        }
    }

    @SneakyThrows
    void eatSynchronized() {
        synchronized (left) {
            synchronized (right) {
                log.info("eat……");
                TimeUnit.SECONDS.sleep(1);
            }
        }
    }

    @SneakyThrows
    void eatReentrantLock() {
        if (leftReentrantLock.tryLock(1, TimeUnit.SECONDS)) {
            try {
                if (rightReentrantLock.tryLock(1, TimeUnit.SECONDS)) {
                    try {
                        log.info("eat……");
                        TimeUnit.SECONDS.sleep(1);
                    } finally {
                        rightReentrantLock.unlock();
                    }
                }
            } finally {
                leftReentrantLock.unlock();
            }
        }
    }

}

@Slf4j
class ChopstickDemo {

}
