package com.yinsongyuan.concurrent.deadlock;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * 死锁Demo
 *
 * @author yinsongyuan
 * @date 2021/3/8
 **/
@Slf4j(topic = "dead.lock")
public class DeadLockDemo {
    public static void main(String[] args) {
        deadLockDemo();
    }

    /**
     * 死锁案例
     * 1. 线程互相占用 对方的锁
     *
     * @author yinsongyuan
     * @date 2021/3/8
     **/
    public static void deadLockDemo() {
        // 锁1
        Object lock1 = new Object();
        // 锁2
        Object lock2 = new Object();
        new Thread(()->{
            synchronized (lock1){
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (lock2){
                    log.info("do something……");
                }
            }
        }, "t1").start();
        new Thread(()->{
            synchronized (lock2){
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (lock1){
                    log.info("do something……");
                }
            }
        }, "t2").start();
    }
}
