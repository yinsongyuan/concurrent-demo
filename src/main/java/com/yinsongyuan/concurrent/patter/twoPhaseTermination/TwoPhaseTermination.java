package com.yinsongyuan.concurrent.patter.twoPhaseTermination;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.util.concurrent.TimeUnit;

/**
 * @author yinsongyuan
 * @date 2021/3/2
 * @description 两段提交终止模式
 * 目的: 在一个线程中结束另一个线程,被结束线程还可以进行异常相关处理
 **/
public class TwoPhaseTermination {
    public static void main(String[] args) throws Exception {
        TPTInterrupt tptInterrupt = new TPTInterrupt();
        tptInterrupt.start();
        TimeUnit.SECONDS.sleep(2);
        tptInterrupt.stop();
    }
}

@Slf4j(topic = "TPTInterrupt")
class TPTInterrupt {
    private Thread thread;

    /**
     * 启动线程方法
     */
    public void start(){
        thread = new Thread(()->{
            while (true){
                Thread currentThread = Thread.currentThread();
                if(currentThread.isInterrupted()){
                    log.info("结束处理！");
                    break;
                }
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    currentThread.interrupt();
                    e.printStackTrace();
                }
            }
        }, "thread");
        thread.start();
    }

    /**
     * 结束线程方法
     */
    public void stop() throws Exception {
        if(ObjectUtils.isEmpty(thread)){
            throw new Exception("该线程没有运行,无法打断");
        }
        thread.interrupt();
    }
}
