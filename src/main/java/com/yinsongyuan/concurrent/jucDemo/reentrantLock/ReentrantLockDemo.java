package com.yinsongyuan.concurrent.jucDemo.reentrantLock;

import lombok.extern.slf4j.Slf4j;
import sun.awt.windows.ThemeReader;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j(topic = "reentrant.lock.demo")
public class ReentrantLockDemo {
    public static void main(String[] args) throws Exception {
//        lock();
//        reentrantLockInterrupt();
//        synchronizedInterrupt();
//        reentrantLockTimeOut();
        reetrantLockFairLock();
    }

    public static void lock() {
        ReentrantLock reentrantLock1 = new ReentrantLock();
        ReentrantLock reentrantLock2 = new ReentrantLock();
        new Thread(() -> {
            reentrantLock1.lock();
            try {
                TimeUnit.SECONDS.sleep(2);
                log.info("test reetrant lock");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                reentrantLock1.unlock();
            }
        }, "t1").start();
        new Thread(() -> {
            reentrantLock2.lock();
            try {
                log.info("test reetrant lock");
            } finally {
                reentrantLock2.unlock();
            }
        }, "t2").start();

    }

    /**
     * reentrantLock 可中断测试
     *
     * @author yinsongyuan
     * @date 2021/3/9
     **/
    public static void reentrantLockInterrupt() {
//        ReentrantLock reentrantLock = new ReentrantLock();
//        Thread t1 = new Thread(() -> {
//            try {
//                reentrantLock.lockInterruptibly();
//                log.debug("获得了锁");
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } finally {
//                reentrantLock.unlock();
//            }
//        }, "t1");
//        t1.start();
//        new Thread(() -> {
//            reentrantLock.lock();
//            try {
//                log.info("reentrant lock test 2");
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                reentrantLock.unlock();
//            }
//        }, "t2").start();
//        log.info("主线程打断");
//        t1.interrupt();

    }

    /**
     * synchronized 可中断测试
     *
     * @author yinsongyuan
     * @date 2021/3/9
     **/
    public static void synchronizedInterrupt() {
        Object lock = new Object();
        Thread t1 = new Thread(() -> {
            synchronized (lock) {
                log.info("测试");
                log.info("测试");
                log.info("测试");
                log.info("测试");
                log.info("测试");
                log.info("测试");
                log.info("测试");
                log.info("测试");
                log.info("测试");
                log.info("测试");
            }
        }, "t1");
        t1.start();
        new Thread(() -> {
            synchronized (lock) {
                log.info("测试");
            }
        }, "t2").start();
        log.info("开始打断");
        t1.interrupt();
    }

    /**
     * reentrantLock 锁超时测试
     *
     * @author yinsongyuan
     * @date 2021/3/9
     **/
    public static void reentrantLockTimeOut() {
        ReentrantLock lock = new ReentrantLock();
        Thread t1 = new Thread(() -> {
            log.debug("启动...");
//            if (!lock.tryLock()) {
//                log.debug("获取立刻失败，返回");
//                return;
//            }
            // 设置时间
            try {
                if (!lock.tryLock(1, TimeUnit.SECONDS)) {
                    log.debug("获取等待 1s 后失败，返回");
                    return;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                log.debug("获得了锁");
            } finally {
                lock.unlock();
            }
        }, "t1");
        lock.lock();
        log.debug("获得了锁");
        t1.start();
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    /**
     * reentrantLock 设置公平锁
     * 默认 非公平锁
     *
     * @author yinsongyuan
     * @date 2021/3/9
     **/
    public static void reetrantLockFairLock() throws InterruptedException {
        ReentrantLock reentrantLock = new ReentrantLock(true);
        reentrantLock.lock();
        for (int i = 0; i < 500; i++) {
            new Thread(() -> {
                reentrantLock.lock();
                try {
                    System.out.println(Thread.currentThread().getName() + " running...");
                } finally {
                    reentrantLock.unlock();
                }
            }, "t" + i).start();
        }
        TimeUnit.SECONDS.sleep(1);
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + " start...");
            reentrantLock.lock();
            try {
                System.out.println(Thread.currentThread().getName() + " running...");
            } finally {
                reentrantLock.unlock();
            }
        }, "强行插入").start();
        reentrantLock.unlock();
    }

}
