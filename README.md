# concurrent-demo

#### 介绍
并发编程代码笔记

#### 代码目录
+ 线程基础
    1. [线程创建](https://gitee.com/yinsongyuan/concurrent-demo/blob/master/src/main/java/com/yinsongyuan/concurrent/threadbase/ThreadCreate.java)
    2. [线程常用API](https://gitee.com/yinsongyuan/concurrent-demo/blob/master/src/main/java/com/yinsongyuan/concurrent/threadbase/ThreadMethod.java)
    3. [Java线程状态转换](https://gitee.com/yinsongyuan/concurrent-demo/blob/master/src/main/java/com/yinsongyuan/concurrent/threadbase/ThreadState.java)
+ Synchronized
    1. [Synchronized基本使用](https://gitee.com/yinsongyuan/concurrent-demo/blob/master/src/main/java/com/yinsongyuan/concurrent/synchronizedDemo/BaseSynchronizedDemo.java)
    2. [Synchronized异常情况](https://gitee.com/yinsongyuan/concurrent-demo/blob/master/src/main/java/com/yinsongyuan/concurrent/synchronizedDemo/SynchronizedExceptionDemo)
    3. [Synchronized案例](https://gitee.com/yinsongyuan/concurrent-demo/blob/master/src/main/java/com/yinsongyuan/concurrent/synchronizedDemo/synchronizedExample/SynchronizedExample.java)
+ Wait/Notify
+ Park/unPark
+ ReentrantLock
+ JDK原子类/原子引用
+ 线程相关应用
    1. [哲学家死锁问题复现](https://gitee.com/yinsongyuan/concurrent-demo/blob/master/src/main/java/com/yinsongyuan/concurrent/deadlock/DiningPhilosophersDemo.java)
+ 线程相关模式
    1. [两阶段终止]
    2. [单例模式]
+ 线程池
    1. [自定义线程池]

